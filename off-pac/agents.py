import numpy as np
from tiles3 import tiles, IHT

# TODO: move tile coder out of the agent

# class offpac_agent:
#     def __init__(self):
#         # For tile coding
#         self.maxSize = 2**12 # Change to a higher number so that the algorithm works with more tiles.
#         self.iht = IHT(self.maxSize)
#         self.num_tiles = 10
#         self.tile_shape = 10
#         self.bounds = [(-2.4, 2.4), (-5, 5), (-41.8, 41.8), (-5, 5)] # TODO: Are these bounds correct?
#         self.scale_factor = [(self.tile_shape)/(b[1]-b[0]) for b in self.bounds]
#
#         self.ev = np.zeros(self.maxSize)
#         self.eu = np.zeros(self.maxSize)
#         self.w = np.zeros(self.maxSize)
#         self.v = np.zeros(self.maxSize)
#         self.u = np.zeros(self.maxSize)
#
#     def start(self, observation):
#         self.last_x = self.transform(observation)
#
#     def learn(self, action, observation, reward, done):
#         x = self.transform(observation)
#         self.last_x = x
#         pass
#
#     def transform(self, observation): # TODO: maybe avoid clipping
#         clipped = [np.clip(obs, self.bounds[i][0], self.bounds[i][1]) for i, obs in enumerate(observation)]
#         scaled = [(x-b[0])*y for x, y, b in zip(clipped, self.scale_factor, self.bounds)]
#         transformed = tiles(self.iht, self.num_tiles, scaled)
#         return transformed



class sarsa_agent:
    def __init__(self, valid_actions = [0,1], maxHashSize = 2**12,
            num_tiles = 10, tile_shape = 10,
            bounds=[(-2.4, 2.4), (-2, 2), (-41.8, 41.8), (-2, 2)],
            stepsize = 0.5, epsilon = 0.0, gamma = 1.0, initial_weights = 1.0):
        self.valid_actions = valid_actions
        # For tile coding
        self.maxSize = maxHashSize # Change to a higher number so that the algorithm works with more tiles.
        self.iht = IHT(self.maxSize)
        self.num_tiles = num_tiles
        self.tile_shape = tile_shape
        self.bounds = bounds # TODO: Are these bounds correct?
        self.scale_factor = [(self.tile_shape)/(b[1]-b[0]) for b in self.bounds]

        self.stepsize = stepsize/self.num_tiles
        self.epsilon = epsilon
        self.gamma = gamma

        self.w = np.zeros(self.maxSize) + initial_weights


    def start(self, observation):
        qs = [self.w[self.transform(observation, a)].sum() for a in self.valid_actions]

        if np.random.rand() < self.epsilon:
            a_ind = np.random.randint(len(self.valid_actions))
        else:
            a_ind = np.argmax(qs)

        action = self.valid_actions[a_ind]
        self.last_x = self.transform(observation, action)
        return action

    def learn(self, last_action, observation, reward, done):
        last_q = self.w[self.last_x].sum()
        if not done:

            qs = [self.w[self.transform(observation, a)].sum() for a in self.valid_actions]

            if np.random.rand() < self.epsilon:
                a_ind = np.random.randint(len(self.valid_actions))
            else:
                a_ind = np.argmax(qs)

            action = self.valid_actions[a_ind]
            q = qs[a_ind]

            self.w[self.last_x] += self.stepsize * (reward + self.gamma * q - last_q)

            self.last_x = self.transform(observation, action)
            return action
        else:
            self.w[self.last_x] += self.stepsize * (reward - last_q)
            return None

    def transform(self, observation, action): # TODO: maybe avoid clipping
        clipped = [np.clip(obs, self.bounds[i][0], self.bounds[i][1]) for i, obs in enumerate(observation)]
        scaled = [(x-b[0])*y for x, y, b in zip(clipped, self.scale_factor, self.bounds)]
        transformed = tiles(self.iht, self.num_tiles, scaled, [action])
        return transformed

class ac_agent:
    def __init__(self, valid_actions = [0,1], maxHashSize = 2**13,
            num_tiles = 10, tile_shape = 10,
            bounds = [(-2.4, 2.4), (-2, 2), (-41.8, 41.8), (-2, 2)],
            gamma = 1.0, a_th = 0.1, a_w = 0.9, lam_th = 0.1, lam_w = 0.3):
        self.valid_actions = valid_actions
        # For tile coding
        self.maxSize = maxHashSize # Change to a higher number so that the algorithm works with more tiles.
        self.iht = IHT(self.maxSize)
        self.num_tiles = num_tiles
        self.tile_shape = tile_shape
        self.bounds = bounds # TODO: Are these bounds correct?
        self.scale_factor = [(self.tile_shape)/(b[1]-b[0]) for b in self.bounds]

        self.gamma = gamma
        self.a_th = a_th/self.num_tiles
        self.a_w = a_w/self.num_tiles
        self.lam_th = lam_th
        self.lam_w = lam_w

        self.th = np.random.uniform(low=-0.01, high=0.01, size=(self.maxSize,))
        self.w = np.random.uniform(low=0.0, high=10.0, size=(self.maxSize,))


    def start(self, observation):

        self.z_th = np.zeros(self.maxSize)
        self.z_w = np.zeros(self.maxSize)
        self.eye = 1.0

        action = np.random.choice(self.valid_actions, p=self.actor(observation))
        self.last_obs = observation
        return action

    def learn(self, last_action, observation, reward, done):

        # print(self.w[:50])
        # print(self.th[:4])
        # print(self.z_w[:50])
        # print(self.z_th[:4])
        # print('--------')

        last_v = self.critic(self.last_obs)
        v = 0.0
        if not done:
            v = self.critic(observation)
        err = reward + self.gamma * v - last_v

        self.z_w = self.gamma * self.lam_w * self.z_w + self.eye * self.critic_grad(self.last_obs)
        self.z_th = self.gamma * self.lam_th * self.z_th + self.eye * self.actor_ln_grad(last_action, self.last_obs)

        self.w += self.a_w * err * self.z_w
        self.th += self.a_th * err * self.z_th

        self.eye *= self.gamma
        self.last_obs = observation

        if not done:
            action = np.random.choice(self.valid_actions, p=self.actor(observation))
            return action

    def actor(self, observation):
        prefs = np.array([self.th[self.transform_actor(observation, a)].sum() for a in self.valid_actions])
        probs = np.exp(prefs)
        probs /= np.sum(probs, axis=0)
        return probs

    def critic(self, observation):
        return self.w[self.transform_critic(observation)].sum()

    def actor_ln_grad(self, action, observation):
        xs = [self.transform_actor(observation, a) for a in self.valid_actions]
        prefs = np.array([self.th[x].sum() for x in xs])
        probs = np.exp(prefs)
        probs /= np.sum(probs, axis=0)

        grad = np.zeros(self.maxSize)
        grad[self.transform_actor(observation, action)] = 1.0

        for x, p in zip(xs, probs):
            grad[x] -= p

        return grad

    def critic_grad(self, observation):
        grad = np.zeros(self.maxSize)
        grad[self.transform_critic(observation)] = 1.0
        return grad

    # TODO: THIS SEPARATE TRANSFORM STUFF IS SO MESSED UP BTW. USE MULTI-HEAD ACTOR.
    def transform_actor(self, observation, action): # TODO: maybe avoid clipping.
        clipped = [np.clip(obs, self.bounds[i][0], self.bounds[i][1]) for i, obs in enumerate(observation)]
        scaled = [(x-b[0])*y for x, y, b in zip(clipped, self.scale_factor, self.bounds)]
        transformed = tiles(self.iht, self.num_tiles, scaled, [action])
        return transformed

    def transform_critic(self, observation): # TODO: maybe avoid clipping
        clipped = [np.clip(obs, self.bounds[i][0], self.bounds[i][1]) for i, obs in enumerate(observation)]
        scaled = [(x-b[0])*y for x, y, b in zip(clipped, self.scale_factor, self.bounds)]
        transformed = tiles(self.iht, self.num_tiles, scaled)
        return transformed
