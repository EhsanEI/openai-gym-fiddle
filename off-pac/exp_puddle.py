import gym
from agents import *
import gym_puddle


env = gym.make('PuddleWorld-v0')
agent = sarsa_agent(valid_actions=list(range(env.action_space.n)),
        maxHashSize = 2**14, bounds=[(0.0,1.0), (0.0,1.0)], epsilon=0.1)

for i_episode in range(10000):
    observation = env.reset() # reset for each new trial
    rreturn = 0
    action = agent.start(observation)
    t = 0
    while True:
        t += 1
        if i_episode > 100:
            env.render()
        # action = env.action_space.sample() # select a random action (see https://github.com/openai/gym/wiki/CartPole-v0)
        observation, reward, done, info = env.step(action)
        rreturn += reward
        # print(t, action, observation, reward, done)
        action = agent.learn(action, observation, reward, done)
        if done:
            print("Episode {} finished after {} timesteps with return {}".format(i_episode, t+1, rreturn))
            break
