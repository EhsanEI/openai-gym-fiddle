import gym
from agents import *

agent = sarsa_agent()

env = gym.make('CartPole-v0')
for i_episode in range(10000):
    observation = env.reset() # reset for each new trial
    action = agent.start(observation)
    t = 0
    while True:
        t += 1
        if i_episode > 10:
            env.render()
        # action = env.action_space.sample() # select a random action (see https://github.com/openai/gym/wiki/CartPole-v0)
        observation, reward, done, info = env.step(action)
        # print(action, observation, reward, done)
        action = agent.learn(action, observation, reward, done)
        if done:
            print("Episode {} finished after {} timesteps".format(i_episode, t+1))
            break
